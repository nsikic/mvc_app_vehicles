using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace monozd.Models
{
    public class VehicleContext : DbContext
    {
        public DbSet<VehicleModel> Models { get; set;}
        public DbSet<VehicleMake> Makers { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite(@"Data Source=/home/nsikic/monozd/Vehicle.db");
    }
}