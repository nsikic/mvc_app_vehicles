using System.ComponentModel.DataAnnotations;

namespace monozd.Models
{
    public class VehicleMake
    {

    
    public int Id { get; set; }
    [Required(ErrorMessage ="Please enter a name")]
    public string Name { get; set; }
    [Required(ErrorMessage ="Please enter an abrivation")]
    public string Abrv { get; set; }

    }

}