using System.ComponentModel.DataAnnotations;

namespace monozd.Models
{
    public class VehicleModel
    {

    
    public int Id { get; set; }
    [Required(ErrorMessage ="Please enter Manufacturer's ID")]
    public int MakeId { get; set; }
    [Required(ErrorMessage ="Please enter name")]
    public string Name { get; set; }
    [Required(ErrorMessage ="Please enter an abrivation")]
    public string Abrv { get; set; }

    }

}