using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using monozd.Models;
namespace monozd.Controllers;

public class VehicleMakeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public VehicleMakeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    public IActionResult CreateVehicleMake()
    {
        List<VehicleMake> manufacturers = new List<VehicleMake>();

        using(var db = new VehicleContext())
        {
            manufacturers = db.Makers.ToList();
        }

        TempData["manufacturers"] = manufacturers;
        return View();
    }



    [HttpPost]
    public IActionResult CreateVehicleMake(VehicleMake manufacturer)
    {
        if(ModelState.IsValid)
        {
        using (var db = new VehicleContext())
            {
            db.Add(manufacturer);
            db.SaveChanges();
            }
        }
        return View();
        
    }

    public IActionResult SearchManufacturer(string searchTerm)
    {
        List<VehicleMake> manufacturers = new List<VehicleMake>();

        using(var db = new VehicleContext())
        {
            manufacturers = db.Makers.Where(u => u.Name.ToLower().Contains(searchTerm.ToLower())).ToList();        }

        TempData["manufacturers"] = manufacturers;

        return View("CreateVehicleMake");
    }

    [HttpPost]
    public IActionResult UpdateManufacturer(VehicleMake manufacturer)
    {
        using (var db = new VehicleContext())
        {
            var manufacturerDb  = db.Makers.Where(u => u.Id == manufacturer.Id).FirstOrDefault();
            TempData["manufacturerDb"] = manufacturerDb;


        }

        return View();
    }

    public IActionResult UpdateManufacturerFinal(VehicleMake manufacturer)
    {
        using (var db = new VehicleContext())
        {
            var manufacturerDb = db.Makers.Where(u =>u.Id == manufacturer.Id).FirstOrDefault();

            manufacturerDb.Name = manufacturer.Name;
            manufacturerDb.Abrv = manufacturer.Abrv;

            db.SaveChanges();
        }

        return View("CreateVehicleMake");
    }

    [HttpPost]
    public IActionResult DeletePost(int id)
    {   
        using (var db = new VehicleContext())
        {
        var manufacturerDb = db.Makers.Find(id);
        
        if(manufacturerDb == null || id == 0)
        {
            return NotFound();
        }    
        
        db.Makers.Remove(manufacturerDb);
        db.SaveChanges();
        
        }
        return RedirectToAction("CreateVehicleMake");
    }
    public IActionResult Delete(int id)
    {
        if(id == 0)
        {
            return NotFound();
        }
        using (var db = new VehicleContext())
        {
        var manufacturerDb = db.Makers.Find(id);

        db.Makers.Remove(manufacturerDb);
        db.SaveChanges();

        }


        return View("CreateVehicleMake");
    }


}
