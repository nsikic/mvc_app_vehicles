using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using monozd.Models;

namespace monozd.Controllers;

public class VehicleModelController : Controller
{
    private readonly ILogger<VehicleModelController> _logger;

    public VehicleModelController(ILogger<VehicleModelController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    public IActionResult CreateVehicleModel()
    {
        List<VehicleModel> models = new List<VehicleModel>();
        
        using(var db = new VehicleContext())
        {
            models = db.Models.ToList();
        }
        TempData["models"] = models;
        return View();
    }

    [HttpPost]
    public IActionResult CreateVehicleModel(VehicleModel model)
    {   
        if(ModelState.IsValid)
        {
        using (var db = new VehicleContext())
            {
            db.Add(model);
            db.SaveChanges();
            }
        }
        return View();
    }

    public IActionResult SearchModel(string searchTerm)
   {
        List<VehicleModel> models = new List<VehicleModel>();

        using(var db = new VehicleContext())
        {
            models = db.Models.Where(u => u.Name.ToLower().Contains(searchTerm.ToLower())).ToList();
        }

        TempData["models"] = models;
        return View("CreateVehicleModel");
    }


      [HttpPost]
    public IActionResult UpdateModel(VehicleModel model)
    {
        using (var db = new VehicleContext())
        {
            var modelDb  = db.Models.Where(u => u.Id == model.Id).FirstOrDefault();
            TempData["modelDb"] = modelDb;
            
        }

        return View();
    }

    public IActionResult UpdateModelFinal(VehicleModel model)
    {
        using (var db = new VehicleContext())
        {
            var UpdateModel = db.Models.Where(u =>u.Id == model.Id).FirstOrDefault();

            UpdateModel.MakeId = model.MakeId;
            UpdateModel.Name = model.Name;
            UpdateModel.Abrv = model.Abrv;

            db.SaveChanges();
        }
        return View("CreateVehicleModel");
    }

    [HttpPost]
    public IActionResult DeletePost(int id)
    {   
        using (var db = new VehicleContext())
        {
        var modelsDb = db.Models.Find(id);
        
        if(modelsDb == null || id == 0)
        {
            return NotFound();
        }    
        
        db.Models.Remove(modelsDb);
        db.SaveChanges();
        
        }
        return RedirectToAction("CreateVehicleModel");
    }
    public IActionResult Delete(int id)
    {
        if(id == 0)
        {
            return NotFound();
        }
        using (var db = new VehicleContext())
        {
        var modelsDb = db.Models.Find(id);

        db.Models.Remove(modelsDb);
        db.SaveChanges();

        }


        return View("CreateVehicleMake");
    }

}
